Source: openjph
Section: libs
Priority: optional
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders: Mathieu Malaterre <malat@debian.org>
Build-Depends: cmake (>= 3.10),
               debhelper (>= 11),
               help2man,
               libtiff-dev,
               ninja-build
Build-Depends-Indep: doxygen
Standards-Version: 4.6.0
Homepage: https://github.com/aous72/OpenJPH
Vcs-Git: https://salsa.debian.org/debian-phototools-team/openjph.git
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/openjph
Rules-Requires-Root: no

Package: libopenjph0.8
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: HTJ2K image compression/decompression library (runtime files)
 Implementation of High-throughput JPEG2000 (HTJ2K), also known as JPH, JPEG2000
 Part 15, ISO/IEC 15444-15, and ITU-T T.814. This library only implements HTJ2K
 only, supporting features that are defined in JPEG2000 Part 1 (for example, for
 wavelet transform, only reversible 5/3 and irreversible 9/7 are supported).
 .
 This package installs the runtime files.

Package: libopenjph-dev
Architecture: any
Section: libdevel
Pre-Depends: ${misc:Pre-Depends}
Depends: libopenjph0.8 (= ${binary:Version}), ${misc:Depends}
Multi-Arch: same
Description: HTJ2K image compression/decompression library (developer files)
 Implementation of High-throughput JPEG2000 (HTJ2K), also known as JPH, JPEG2000
 Part 15, ISO/IEC 15444-15, and ITU-T T.814. This library only implements HTJ2K
 only, supporting features that are defined in JPEG2000 Part 1 (for example, for
 wavelet transform, only reversible 5/3 and irreversible 9/7 are supported).
 .
 This package installs the development files.

Package: openjph-tools
Architecture: any
Section: graphics
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: HTJ2K image compression/decompression library (command line tools)
 Implementation of High-throughput JPEG2000 (HTJ2K), also known as JPH, JPEG2000
 Part 15, ISO/IEC 15444-15, and ITU-T T.814. This library only implements HTJ2K
 only, supporting features that are defined in JPEG2000 Part 1 (for example, for
 wavelet transform, only reversible 5/3 and irreversible 9/7 are supported).
 .
 This package installs the command line utilities.

Package: openjph-doc
Architecture: all
Section: doc
Depends: doc-base, ${misc:Depends}
Description: HTJ2K image compression/decompression library (documentation files)
 Implementation of High-throughput JPEG2000 (HTJ2K), also known as JPH, JPEG2000
 Part 15, ISO/IEC 15444-15, and ITU-T T.814. This library only implements HTJ2K
 only, supporting features that are defined in JPEG2000 Part 1 (for example, for
 wavelet transform, only reversible 5/3 and irreversible 9/7 are supported).
 .
 This package installs the documentation files.
