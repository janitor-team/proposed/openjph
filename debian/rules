#!/usr/bin/make -f
#export DH_VERBOSE=1

include /usr/share/dpkg/default.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DPKG_GENSYMBOLS_CHECK_LEVEL = 4

%:
	dh $@ --buildsystem=cmake+ninja

ifeq ($(DEB_BUILD_ARCH),$(filter $(DEB_BUILD_ARCH),amd64 x32))
  CMAKE_EXTRA_FLAGS += -DOJPH_DISABLE_INTEL_SIMD:BOOL=OFF
else
  CMAKE_EXTRA_FLAGS += -DOJPH_DISABLE_INTEL_SIMD:BOOL=ON
endif

# upstream requires amd64 binaries from kakadu (non-free) to run non-regression
# testing. Make sure to set BUILD_TESTING to OFF for now:
ifeq (,$(filter check,$(OPENJPH_BUILD_TESTING)))
  BUILD_TESTING=OFF
else
  BUILD_TESTING=ON
endif

CMAKE_EXTRA_FLAGS += \
	-DBUILD_SHARED_LIBS:BOOL=ON \
	-DBUILD_TESTING:BOOL=$(BUILD_TESTING) \

override_dh_auto_configure:
	dh_auto_configure -- $(CMAKE_EXTRA_FLAGS)

override_dh_auto_build-indep:
	(cd docs && doxygen)

override_dh_auto_install-indep:

override_dh_auto_test-indep:

override_dh_clean-indep:
	dh_clean docs/html/

override_dh_installdocs-arch: manpages
	dh_installdocs

override_dh_auto_test-arch:
ifeq (,$(filter check,$(OPENJPH_BUILD_TESTING)))
else
	dh_auto_test
endif

override_dh_clean-arch:
	dh_clean bin/ojph_compress
	dh_clean bin/ojph_expand
	dh_clean bin/openjph.pc
	dh_clean bin/libopenjph.*
	dh_clean debian/*.1

debian/ojph_compress.1: debian/ojph_compress.1.in
	LD_PRELOAD= LD_LIBRARY_PATH=./debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH) help2man --version-string=$(DEB_VERSION_UPSTREAM) --help-option=" " --include=$< --output=$@ --no-info --no-discard-stderr ./debian/tmp/usr/bin/ojph_compress

debian/ojph_expand.1: debian/ojph_expand.1.in
	LD_PRELOAD= LD_LIBRARY_PATH=./debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH) help2man --version-string=$(DEB_VERSION_UPSTREAM) --help-option=" " --include=$< --output=$@ --no-info --no-discard-stderr ./debian/tmp/usr/bin/ojph_expand

.PHONY: manpages

manpages: debian/ojph_compress.1 debian/ojph_expand.1
